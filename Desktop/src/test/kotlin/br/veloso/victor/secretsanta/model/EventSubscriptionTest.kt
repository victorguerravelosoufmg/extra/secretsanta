package br.veloso.victor.secretsanta.model

import java.time.LocalDateTime
import kotlin.test.*

class EventSubscriptionTest {
    var event: Event =
        Event(
            "TestEvent",
            LocalDateTime.MAX
        )

    fun publish() {
        event.url = "http://some.random.url"
        event.publish()
    }

    @Test
    fun testGetNonExistentParticipant() {
        publish()
        assertNull(event.getParticipant("TestNonExistentUsername"))
    }

    @Test
    fun testDraftEventSubscription() {
        assertFailsWith<IllegalStateException> {
            val account = DumbAccount("TestUser")
            event.subscribe(account)
        }
    }

    @Test
    fun testClosedEventSubscription() {
        publish()
        event.draw()

        assertFailsWith<IllegalStateException> {
            val account = DumbAccount("TestUser")
            event.subscribe(account)
        }
    }

    @Test
    fun testPublishedEventSubscription() {
        val account = DumbAccount("TestUser")
        publish()
        try {
            event.subscribe(account)
            assertEquals(event.getParticipant(account.name)!!, account)
        } catch (e: IllegalStateException) {
            fail("No exception should be thrown at subscription on a published event")
        }
    }
}