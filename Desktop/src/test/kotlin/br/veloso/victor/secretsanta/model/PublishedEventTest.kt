package br.veloso.victor.secretsanta.model

import java.time.LocalDateTime
import kotlin.test.Test
import kotlin.test.assertEquals

class PublishedEventTest {
    var event: Event =
        Event(
            "TestEvent",
            LocalDateTime.MAX,
            "Free-form text"
        )

    private val participants = arrayListOf<Account>(
        DumbAccount("A"),
        DumbAccount("B"),
        DumbAccount("C"),
        DumbAccount("D"),
        DumbAccount("E"),
        DumbAccount("F"),
        DumbAccount("G")
    )

    init {
        event.url = "http://some.random.url"
        event.publish()
    }

    @Test
    fun testEventWithSingleParticipant() {
        val participant = participants.first()

        event.subscribe(participant)

        event.draw()

        assertEquals(participant, participant.drawn)

        assertEquals(event.getParticipant(participant.name), participant)
    }

    @Test
    fun testDrawEvent() {
        val drawnParticipants = setOf<String>()
        val drawerParticipants = setOf<String>()

        participants.forEach {
            event.subscribe(it)
        }

        event.draw()
        participants.forEach {
            drawerParticipants.plus(event.getParticipant(it.name)!!.name)
            drawnParticipants.plus(event.getParticipant(it.name)!!.drawn!!.name)
        }

        assertEquals(drawnParticipants, drawerParticipants)

        participants.forEach {
            drawnParticipants.contains(it.name)
            drawerParticipants.contains(it.name)
        }
    }
}