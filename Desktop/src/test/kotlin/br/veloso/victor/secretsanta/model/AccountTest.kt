package br.veloso.victor.secretsanta.model

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class AccountTest {
    private val name = "TestUser"
    private val password = "TestPassword"
    private val account = Account(name, password)
    private val drawnAccount = Account("AnotherUser", password)

    private fun loginAccount() {
        try {
            account.login(password)
        } catch (ex: LoginFailedException) {
            fail("Correct password login mustn't throw any exceptions")
        }
    }

    private fun accountUnloggedInPermissions() {
        assertFailsWith<IllegalStateException> {
            account.contact = DumbContact()
        }
        try {
            val con = account.contact
            con.toString()
        } catch (ex: Exception) {
            fail("Obtaining account's contact must be allowed for unlogged users.")
        }
        try {
            account.setDrawn(drawnAccount)
        } catch (ex: Exception) {
            fail("Unlogged user's drawn account must be modifiable.")
        }
        assertFailsWith<IllegalStateException> {
            val drawn = account.drawn
            drawn.toString()
        }
        assertEquals(name, account.name)

        try {
            account.sendResults()
        } catch (ex: Exception) {
            fail("SendResults must be allowed for unlogged users.")
        }
    }

    @Test
    fun testUnloggedPermissions() {
        accountUnloggedInPermissions()
    }

    @Test
    fun testAfterLogoutPermissions() {
        loginAccount()
        account.logout()
        accountUnloggedInPermissions()
    }

    @Test
    fun testWrongCredentialsLogin() {
        assertFailsWith<LoginFailedException> {
            account.login("Wrong password")
        }
    }

    @Test
    fun testLoginAccount() {
        loginAccount()
        account.logout()
    }

    @Test
    fun testAccountLoggedInPermissions() {
        loginAccount()
        try {
            account.contact = DumbContact()
            val con = account.contact
            con.toString()
            account.setDrawn(drawnAccount)
            val drawn = account.drawn
            drawn.toString()
            account.sendResults()
        }
        catch (ex: Exception) {
            fail("Logged in users must have permission for all operations.")
        }
    }
}