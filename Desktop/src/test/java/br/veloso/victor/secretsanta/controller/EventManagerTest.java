package br.veloso.victor.secretsanta.controller;

import br.veloso.victor.secretsanta.model.Account;
import br.veloso.victor.secretsanta.model.Event;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.*;

public class EventManagerTest {
    private EventManager manager;

    @Before
    public void setUp() throws Exception {
        manager = new EventManager();
    }

    @Test
    public void testCreateEvent() {
        Event event = manager.createEvent("name", LocalDateTime.MAX);

        Assert.assertEquals(event, manager.getEvent(event.getUrl()));

        try {
            event.publish();
        } catch (IllegalStateException exception) {
            exception.printStackTrace();
            Assert.fail(exception.getMessage());
        }
    }

    @Test
    public void testGenerateUrl() {
        Event event = manager.createEvent("name", LocalDateTime.MAX);
        String url = Objects.requireNonNull(event.getUrl());
        Assert.assertTrue("Event's URL has invalid format: " + url, url.matches("^https?://\\w+[\\s\\S]*"));
    }

    @Test
    public void testUrlUniqueness() {
        Collection<String> eventsUrl = new HashSet<>();
        for (int i = 0; i < 200000; i++) {
            Event newEvent = manager.createEvent("name", LocalDateTime.MAX);
            eventsUrl.add(newEvent.getUrl());
        }

        Assert.assertEquals(200000, eventsUrl.size());
    }

    @Test
    public void testDrawScheduling() {
        Event event = manager.createEvent("name", LocalDateTime.now().plusSeconds(3));
        event.publish();
        try {
            int i = 0;
            while (true) {
                i++;
                event.subscribe(new Account("name ".concat(Integer.toString(i)),"password"));
                Thread.sleep(1000L);
            }
        } catch (IllegalStateException exception) {
            Assert.assertTrue(true);
        } catch (InterruptedException exception) {
            Assert.fail("Thread sleeping interrupted!");
        }

    }
}
