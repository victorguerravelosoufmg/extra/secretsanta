package br.veloso.victor.secretsanta.model


class LoginFailedException(message: String?) : RuntimeException(message)

open class Account(val name: String, private val password: String) {
    var contact: Contact? = null
        set(value) {
            if (isLogged) field = value
            else throw IllegalStateException("This operation requires the account to be logged in.")
        }

    private var _drawn: Account? = null

    val drawn: Account?
        get(){
            if (isLogged) return _drawn
            else throw IllegalStateException("This operation requires the account to be logged in.")
        }

    private var isLogged: Boolean = false

    fun setDrawn(account: Account?) {
        if (account != null) {
            _drawn = account
        }
    }

    fun login(password: String) {
        if (password != this.password) {
            throw LoginFailedException("Wrong password.")
        }
        else isLogged = true
    }

    fun logout() {
        isLogged = false
    }

    fun sendResults() {
        _drawn?.name?.let { contact?.send(it) }
    }
}
