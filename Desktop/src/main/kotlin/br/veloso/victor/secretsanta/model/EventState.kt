package br.veloso.victor.secretsanta.model

interface EventState {
    fun handleSubscription()
    fun handleRedefinition() {
        throw IllegalStateException("Changes are not allowed after publishing events")
    }
}

class DraftEventState: EventState {
    override fun handleSubscription() {
        throw IllegalStateException("Event's in draft state. Please publish it before trying account subscription.")
    }

    override fun handleRedefinition() = Unit

}

class PublishedEventState: EventState {
    override fun handleSubscription() = Unit // Do nothing...
}

class ClosedEventState: EventState {
    override fun handleSubscription() {
        throw IllegalStateException("Event's already closed.")
    }
}