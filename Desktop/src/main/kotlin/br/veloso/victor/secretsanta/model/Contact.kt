package br.veloso.victor.secretsanta.model

interface Contact {
    fun send(message: String)
}

class EmailContact(val email: String) : Contact{
    override fun send(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class PhoneContact(val phone: String) : Contact{
    override fun send(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}