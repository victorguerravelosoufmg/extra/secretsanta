package br.veloso.victor.secretsanta.model

import java.time.LocalDateTime
import java.time.temporal.Temporal
import kotlin.RuntimeException

class Event(val name: String, val deadline: Temporal) {
    private val subscriptions: MutableMap<String, Account> = mutableMapOf()
    private var state: EventState = DraftEventState()
    var url: String? = null
        set(value) {
            handleRedefinition {
                field = value
            }
        }

    var rules: String = ""
        set(value) {
            handleRedefinition {
                field = value
            }
        }

    constructor(name: String, deadline: Temporal, rules: String) : this(name, deadline) {
        this.rules = rules
    }

    private fun handleRedefinition(definition: () -> Unit) {
        try {
            state.handleRedefinition()
            definition()
        } catch (exception: IllegalStateException) {
            throw exception
        }
    }

    fun getParticipant(name: String): Account? = subscriptions[name]

    fun subscribe(participant: Account) {
        try {
            state.handleSubscription()
            val oldSubscription = subscriptions.putIfAbsent(participant.name, participant)
            if (oldSubscription != null) {
                throw RuntimeException("The username \"${participant.name}\" is already taken!")
            }
        } catch (exception: IllegalStateException) {
            throw exception
        }
    }

    fun publish() {
        if (url.isNullOrBlank()) {
            throw IllegalStateException("Event's URL not set.")
        } else {
            state = PublishedEventState()
        }
    }

    fun draw() {
        var previous: String = ""

        val shuffledSubscriptions = subscriptions.asIterable().shuffled()

        shuffledSubscriptions.forEach { entry: Map.Entry<String, Account> ->
            if (previous != "") {
                entry.value.setDrawn(subscriptions.get(previous))
            }
            previous = entry.key
        }

        try {
            shuffledSubscriptions.first().value.setDrawn(subscriptions.get(previous))
        }
        catch (exception: NoSuchElementException) {
            System.err.println("Event closed without any account subscriptions")
        }

        state = ClosedEventState()
    }
}