package br.veloso.victor.secretsanta.controller;

import br.veloso.victor.secretsanta.model.Event;
import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

class UrlGenerator {
    private final String basename;
    private final Event event;
    private final MessageDigest md5 = getDigest();
    private final Random random = new Random();


    UrlGenerator(String basename, Event event) {
        this.basename = basename;
        this.event = event;
    }

    @NotNull
    String generateUrl() {
        return basename + "/" + event.getName() + generateUrlHash();
    }
    @NotNull
    private String generateUrlHash() {

        int rawHashCode = getRawHash();

        return Integer.toString(rawHashCode);
    }

    private int getRawHash() {
        byte[] hashSeed = (event.getName() + random.nextInt()).getBytes();

        md5.update(hashSeed);

        return Math.abs(Arrays.hashCode(md5.digest()));
    }

    @NotNull
    private MessageDigest getDigest() {
        MessageDigest md5 = null;

        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Objects.requireNonNull(md5);
    }

}