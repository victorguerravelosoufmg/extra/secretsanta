package br.veloso.victor.secretsanta.controller;

import br.veloso.victor.secretsanta.model.Event;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class EventManager {
    private HashMap<String, Event> events;
    private final String basename;
    private final ScheduledExecutorService scheduler;

    public EventManager() {
        this("");
    }

    public EventManager(String basename) {
        events = new HashMap<>();
        this.basename = basename.equals("") ? "http://localhost" : basename;
        scheduler = Executors.newScheduledThreadPool(2);
    }

    @NotNull
    public Event createEvent(String name, Temporal deadline) {
        Event event = new Event(name, deadline);

        scheduler.schedule(
                event::draw,
                ChronoUnit.SECONDS.between(LocalDateTime.now(), deadline),
                TimeUnit.SECONDS
        );

        String url = generateUniqueUrl(event);

        events.put(url, event);

        return event;
    }

    private String generateUniqueUrl(Event event) {
        String url;

        UrlGenerator urlGenerator = new UrlGenerator(this.basename, event);

        do {
            url = urlGenerator.generateUrl();
        } while (!validateUrl(url));

        event.setUrl(url);

        return url;
    }

    private boolean validateUrl(String url) {
        return !events.containsKey(url);
    }

    public Event getEvent(String url) {
        return events.get(url);
    }
}
